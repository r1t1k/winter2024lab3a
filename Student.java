public class Student{
	public String program;
	public String name;
	public int age;

	public void sayProgram(){
		System.out.println(this.name + " is in the " + this.program + " Program.");
	}
	
	public void drinkingAge(){
		if(this.age >= 18){
			System.out.println(this.name + " is " + this.age + " years old and can legally drink alcohol.");
		}
		else{
			System.out.println(this.name + " is only " + this.age + " years old and is not allowed to drink alcohol.");
		}
		
	}
}