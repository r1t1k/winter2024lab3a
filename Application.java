public class Application{
	public static void main(String[] args){
		Student firstStudent = new Student();
		Student secondStudent = new Student();
		
		firstStudent.program = "Computer Science";
		firstStudent.name = "Ritik";
		firstStudent.age = 17;
		
		secondStudent.program = "Social Science";
		secondStudent.name = "Liam";
		secondStudent.age = 18;
		
		firstStudent.sayProgram();
		secondStudent.sayProgram();
		firstStudent.drinkingAge();
		secondStudent.drinkingAge();
		
		Student[] section4 = new Student[3];
		section4[0] = firstStudent;
		section4[1] = secondStudent;
		section4[2] = new Student();
		section4[2].program = "Pure and Applied Science";
		section4[2].name = "Yanan";
		section4[2].age = 18;
		System.out.println(section4[0].program);
		System.out.println(section4[2].program);
	}
}